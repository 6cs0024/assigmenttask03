package base;

public class Controller {
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void showView() {
        view.setVisible(true);
    }

    public void sendNameToBackEnd(String name) {
        model.setPlayerName(name);
        model.sendNameToBackEnd(name);
    }
}
