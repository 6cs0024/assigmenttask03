package base;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private String playerName;
    private List<Observer> observers = new ArrayList<>();

    public Model() {
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
        notifyObservers();
    }

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    private void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(playerName);
        }
    }

    public void sendNameToBackEnd(String name) {
        System.out.printf("Welcome %s! Thanks for playing Abominodo.\n ***PRESS ENTER TO CONTINUE THE GAME***", name);
    }
}
