package base;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame implements Observer {
    private JTextField nameField;
    private JButton submitButton;

    private Model modelInterface = new Model();
    private Controller controllerInterface = new Controller(modelInterface, this);

    public View() {
        setTitle("Abominodo - The Best Dominoes Puzzle Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        // Display a welcoming message with information about the game
        JOptionPane.showMessageDialog(this,
                "Welcome to Abominodo - The Best Dominoes Puzzle Game in the Universe.\n"
                        + "Version 2.1 (c), Kevan Buckley, 2014");

        // User input panel
        JPanel inputPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel inputLabel = new JLabel("Enter Your Name:");
        nameField = new JTextField(20);
        submitButton = new JButton("Submit");
        submitButton.addActionListener(e -> handleSubmission());
        inputPanel.add(inputLabel);
        inputPanel.add(nameField);
        inputPanel.add(submitButton);

        add(inputPanel, BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        // Register the view as an observer
        modelInterface.addObserver(this);
    }

    @Override
    public void update(String playerName) {
        nameField.setText(playerName);
    }

    private void handleSubmission() {
        String name = nameField.getText();

        if (name.trim().isEmpty()) {
            // Show an error message if the name is empty or contains only spaces
            JOptionPane.showMessageDialog(this, "Please enter a valid name.", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            // Notify the backend with the player's name
            controllerInterface.sendNameToBackEnd(name);

            // Display a thank you message and close the window
            JOptionPane.showMessageDialog(this, "Thank you, " + name + "! Enjoy the game.", "Welcome",
                    JOptionPane.INFORMATION_MESSAGE);
            dispose();
        }
    }
}
